package ttn.ftel.streamapp.livestream;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import ttn.ftel.streamapp.R;
import ttn.ftel.streamapp.lib.funsdk.support.FunSupport;
import ttn.ftel.streamapp.lib.funsdk.support.OnFunDeviceOptListener;
import ttn.ftel.streamapp.lib.funsdk.support.models.FunDevice;
import ttn.ftel.streamapp.lib.funsdk.support.widget.FunVideoView;
import ttn.ftel.streamapp.lib.sdk.struct.H264_DVR_FILE_DATA;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.view.MotionEvent;
import android.view.View;

public class LiveStreamActivity extends AppCompatActivity implements OnFunDeviceOptListener,
        OnPreparedListener,
        OnErrorListener,
        OnInfoListener {

    FunVideoView funVideoView;
    FunDevice funDevice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_camera);
        funVideoView = (FunVideoView) findViewById(R.id.funVideoView);
        // 注册设备操作回调
        FunSupport.getInstance().registerOnFunDeviceOptListener(this);

        funDevice = FunSupport.getInstance().findDeviceById(1139894344);

        if (null == funDevice) {
            finish();
            return;
        }

//        if (!funDevice.hasLogin() || !funDevice.hasConnected()) {
//            loginDevice();
//        } else {
//            requestSystemInfo();
//            requestChannelSystemFunction();
//        }

        funVideoView.setOnPreparedListener(this);
        funVideoView.setOnErrorListener(this);
        funVideoView.setOnInfoListener(this);
    }

    @Override
    public void onDeviceLoginSuccess(FunDevice funDevice) {

    }

    @Override
    public void onDeviceLoginFailed(FunDevice funDevice, Integer errCode) {

    }

    @Override
    public void onDeviceGetConfigSuccess(FunDevice funDevice, String configName, int nSeq) {

    }

    @Override
    public void onDeviceGetConfigFailed(FunDevice funDevice, Integer errCode) {

    }

    @Override
    public void onDeviceSetConfigSuccess(FunDevice funDevice, String configName) {

    }

    @Override
    public void onDeviceSetConfigFailed(FunDevice funDevice, String configName, Integer errCode) {

    }

    @Override
    public void onDeviceChangeInfoSuccess(FunDevice funDevice) {

    }

    @Override
    public void onDeviceChangeInfoFailed(FunDevice funDevice, Integer errCode) {

    }

    @Override
    public void onDeviceOptionSuccess(FunDevice funDevice, String option) {

    }

    @Override
    public void onDeviceOptionFailed(FunDevice funDevice, String option, Integer errCode) {

    }

    @Override
    public void onDeviceFileListChanged(FunDevice funDevice) {

    }

    @Override
    public void onDeviceFileListChanged(FunDevice funDevice, H264_DVR_FILE_DATA[] datas) {

    }

    @Override
    public void onDeviceFileListGetFailed(FunDevice funDevice) {

    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    @Override
    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {

    }
}
